# [0.21.0](https://gitlab.com/lameimpala/mose-coffee/compare/v0.20.3...v0.21.0) (2023-07-05)


### Features

* add pic of the man himself ([ba2cc48](https://gitlab.com/lameimpala/mose-coffee/commit/ba2cc48a5f37f20801f3c9f3d01cfb69762dbb7b))

## [0.20.3](https://gitlab.com/lameimpala/mose-coffee/compare/v0.20.2...v0.20.3) (2023-07-05)


### Bug Fixes

* region ([1d3a5db](https://gitlab.com/lameimpala/mose-coffee/commit/1d3a5dbf0655a1f2a7793c89a037b11fb41aeff7))

## [0.20.2](https://gitlab.com/lameimpala/mose-coffee/compare/v0.20.1...v0.20.2) (2023-07-05)


### Bug Fixes

* remove favicon.ico ([7411319](https://gitlab.com/lameimpala/mose-coffee/commit/74113195772e6269099e427d200c39853ca096e2))

## [0.20.1](https://gitlab.com/lameimpala/mose-coffee/compare/v0.20.0...v0.20.1) (2023-07-05)


### Bug Fixes

* remove favicon.ico ([3dd2d05](https://gitlab.com/lameimpala/mose-coffee/commit/3dd2d052c81fae92bd909c23bc47ea281626119a))

# [0.20.0](https://gitlab.com/lameimpala/mose-coffee/compare/v0.19.0...v0.20.0) (2023-07-05)


### Features

* initial commit after fork ([28ed75e](https://gitlab.com/lameimpala/mose-coffee/commit/28ed75e97516cad93c76323ebeb9ede7eb5b861c))

# [0.19.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.18.0...v0.19.0) (2023-04-02)


### Features

* increase top artist count ([6a799c7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/6a799c7210ca23e3333a5756fb74b2c63007145e))

# [0.18.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.17.0...v0.18.0) (2023-03-28)


### Bug Fixes

* use node instead of npm start in docker cmd ([bc3c1a7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/bc3c1a7e0fab2e5d3067901aa2ab4bbf3c7044aa))


### Features

* improve responsiveness with columns for songs and artists ([4da6f2c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/4da6f2c75f833e5f83efbfbd78257fa030765aa6))

# [0.17.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.16.0...v0.17.0) (2023-03-25)


### Bug Fixes

* linting ([4a22d6a](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/4a22d6aa19883912684268ed8f7f07007f35fc85))


### Features

* add top artists and refactor ([d4658c2](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/d4658c20e4d16b990bb8f63c633857c41429af2b))

# [0.16.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.15.0...v0.16.0) (2022-09-19)


### Features

* change style of plays ([aefcc3c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/aefcc3c42fe58e15b5e681261ac2bb74470a8a49))
* change style of plays ([327742c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/327742c7e461756dcac9de701a9b0f23f29045dc))

# [0.15.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.14.1...v0.15.0) (2022-09-19)


### Features

* change play count to 60 ([8af34eb](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/8af34eb4df833a2ce8c7d474dda1e85fc3908206))

## [0.14.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.14.0...v0.14.1) (2022-09-19)


### Bug Fixes

* fix tsc errors ([7804ae7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/7804ae7ed175e829ad2eb7535763b3c3f9b9f237))

# [0.14.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.13.0...v0.14.0) (2022-09-19)


### Features

* add modal ([cf88d26](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/cf88d26c14ab2183272f587d51322bb843f09460))

# [0.13.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.12.0...v0.13.0) (2022-09-19)


### Features

* remove blog and update readme ([379269d](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/379269dd2796e34aef9fa0e25fbbe6f872721089))

# [0.12.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.11.0...v0.12.0) (2022-08-06)


### Features

* change Plays to 4-wide columns ([60da6c7](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/60da6c747018c3fe50e6a0b3a959f37e8e57288f))

# [0.11.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.10.0...v0.11.0) (2022-07-31)


### Features

* include play counts ([08cd53c](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/08cd53c28497c978d0914ccfe87e147cbee71b12))

# [0.10.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.9.0...v0.10.0) (2022-07-31)


### Features

* split cards into 2 columns ([536342a](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/536342ac6ad88b358cddef514241880f0bdc7fe6))

# [0.9.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.8.0...v0.9.0) (2022-07-31)


### Features

* add links ([65b3736](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/65b3736f7565496f74f5c7dccab51210db15b406))

# [0.8.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.7.0...v0.8.0) (2022-07-31)


### Features

* add favicon ([0797dd0](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/0797dd0746bf3109b2ebb029c4fa9d4d835e3024))

# [0.7.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.6.1...v0.7.0) (2022-07-31)


### Features

* add fontawesome ([737493f](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/737493f966105f43639df6f2f4cbc41078d48e12))

## [0.6.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.6.0...v0.6.1) (2022-07-31)


### Bug Fixes

* left align cards ([572c1d1](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/572c1d1050def7e6e2269cc31db55a60a097253f))

# [0.6.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.5.0...v0.6.0) (2022-07-31)


### Features

* improve styling of landing page ([5475161](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/5475161b03b2568cbae7bbe4286b1d38f80edec4))

# [0.5.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.4.0...v0.5.0) (2022-07-30)


### Features

* fetch plays from api ([147aafc](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/147aafc259ddf97c1b8db4b61f770da20a8b0f6e))

# [0.4.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.3.2...v0.4.0) (2022-07-30)


### Features

* add post ([6e3ba20](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/6e3ba2087bcdee4cd1d3d8d4cae62fa4fd8667e3))

## [0.3.2](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.3.1...v0.3.2) (2022-07-30)


### Bug Fixes

* build as prod ([da5cb27](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/da5cb27bb00e1f2a14b8746a00e6c5bb2cb7ff65))

## [0.3.1](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.3.0...v0.3.1) (2022-07-30)


### Bug Fixes

* remove ci skipper from auto tagging ([ee1c7e5](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/ee1c7e53ca72928f4b44e5c6ff9ca1e591495e5e))

# [0.3.0](https://gitlab.com/lameimpala/whats-reid-listening-to/compare/v0.2.0...v0.3.0) (2022-07-30)


### Bug Fixes

* add changelog to tagging ([5844401](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/5844401a62a9c213500053a99b099757016bbc72))
* change order of plugins ([0317dc5](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/0317dc5295e20d4808e05b6089a12a98b2fdea71))
* disable husky in CI ([2a28160](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/2a2816057eab64129bb55f2dae53364d38bb9a44))
* install dependencies ([109a2f6](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/109a2f68a7d12a7abcce9eb65ecb61beb3cb72bc))
* use tag instead of latest when deploying image ([7e8b415](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/7e8b4156348b74308d6c6ec6535b9e351151ab52))


### Features

* bump package.json when creating tags ([b64adf9](https://gitlab.com/lameimpala/whats-reid-listening-to/commit/b64adf902d78e9fc136e7c58441a6b548ca9bb67))
