import MosePic from "../mose.jpg";

export function App() {
  return (
    <div className="container">
      <p>Hello World!! This is Mose Coffee</p>
      <img src={MosePic} />
    </div>
  );
}
