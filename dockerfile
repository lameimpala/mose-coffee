FROM node:latest

WORKDIR /usr/app

ADD ./ ./

RUN npm i && NODE_ENV=production npm run build && npm prune --production

ENV NODE_ENV "production"

EXPOSE 8080

CMD ["node", "./dist/back"]